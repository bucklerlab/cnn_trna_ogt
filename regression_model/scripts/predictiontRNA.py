from keras.models import model_from_json
import pandas as pd
import numpy as np
from keras import backend as K
import tensorflow as tf



# We need some global variables. Because couldn't pass them to Hyperopt.

global maxtRNALen
global tRNA
global ValSpecies
global allSpecies
global YTarget
global trainSpecies
global epochs
global sampleIm
global path1
global path2

# Tensorflow Configirations
config = tf.ConfigProto(allow_soft_placement=True)
config.gpu_options.per_process_gpu_memory_fraction = 0.4
sess = tf.Session(config=config)
K.set_session(sess)



def prepareData():
    # Takes two variables that indicates if we use archaea or bacteria
    # Returns read data as pandas data frame

    d = readData(path1)
    asize=d.shape[0]

    return d, asize

def getTensor(Data, whichColumn,mxlen ):
    # Takes data as pandas data frame
    # Returns selected column as tensor. Selected column should have RNA sequences. Also returns max lenght of the RNA
    SEQs = getColumn(Data, Col=whichColumn)
    # Convert SEQs data frame to CNN compatible one-hot encoded.
    SEQTensor = getOneHotEncodedSet(SEQs, mxlen=mxlen)

    return SEQTensor, mxlen



def readData(path, deli="\t", head=0):
    # Takes path and returns read data as pandas data frame
    # head indicates number of rows should be skipped for reading. 0 skips first row. -1 does not skip any row. Optional
    # deli shows delimmiter. Optional.

    return pd.read_csv(path, header=head,delimiter = deli)
def concatDf(dfList):
    # Concats list of data frames into one data frame
    df = dfList[0]
    for l in range(1, len(dfList)):
        df = pd.concat([df, dfList[l]], axis=0)
    return pd.DataFrame(df.values)

def getColumn(df, Col):
    # Takes a pandas data frame and returns a column
    res = df[df.columns[Col]]
    return pd.DataFrame(res.values)


def getOneHotEncodedSet(SEQs, mxlen, depth=4):
    # Takes sqquence list, and max lenght of sequences
    # Returns one hot encoded sequence list. Shorter sequences are 0 padded.
    one_hot_seqs = []
    for seq in SEQs.values[:, 0]:
        one_hot_seqs.append(one_hot_encoding(seq, mxlen))

    one_hot_seqs = np.array(one_hot_seqs)
    return  one_hot_seqs.reshape(one_hot_seqs.shape[0], depth, mxlen, 1)
def one_hot_encoding(seq, mx):
    # Takes a sequence and max lenght
    # Returns one hot encoded 2-dimensional array.
    dict = {'A': [1, 0, 0, 0], 'C': [0, 1, 0, 0], 'G': [0, 0, 1, 0], 'T': [0, 0, 0, 1]}
    one_hot_encoded = np.zeros(shape=(4, mx))

    for i, nt in enumerate(seq):

        if nt.upper() == "A" or nt.upper() == "C" or nt.upper() == "G" or nt.upper() == "T" :
            one_hot_encoded[:,i] = dict[nt.upper()]
        else:
            continue
    return one_hot_encoded


def writefile(P, S, pth):
    # P is predictions, R is real values, S is species array. Pth is path.
    for i in range(len(P)):
        with open(pth , "a") as myfile:
            mystr = S[i] + "," + str(P[i]) + "\n"
            myfile.write(mystr)
    return

def loadModel(path, modelname):
    json_file = open(path+modelname+".json", 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(path+modelname+".h5")
    print("Loaded model from disk")
    return loaded_model

####################################   START     ##############################################################
####################################   INPUTS    ###############################################################




# Initializations

trnaSeqColumn=10 # in data file
speciesColumn = 1 # in data file

#Input files

#tRNAPath
path1 = "/users/emre/Documents/Archaea_RNAs_wOGT.csv"

#trained Model path and name
ModelPath = "/users/emre/Documents/"
ModelName ="Archaea-BacteriaModel"

# provide path for output
path ="/users/emre/Documents/predictions.txt"



####################################end of input parameters#############################################################
##########################################################################################################################
##########################################################################################################################


maxtRNALen=0 # do not change
# Read data from files
Data, dataSize = prepareData()


myModel=loadModel(ModelPath,ModelName)


tRNA, maxtRNALen = getTensor(Data, trnaSeqColumn, int(myModel.input.shape[2]))


mySpecies = Data[Data.columns[speciesColumn]]
myUniqueSpecies = mySpecies.unique()


PR=[] # to keep all predictions
Spec=[] # tested species list, relates to AllPR and AllREAL


for v in myUniqueSpecies:  # Get predictions on validation set.
    specContTest = (mySpecies == v)  # True-False array that shows testing set
    PR.append(np.median(myModel.predict(tRNA[specContTest]))) # Species OGT prediction is obtained as median of RNA related predictions
    Spec.append(v)

AllPR=np.array(PR).ravel()

# Write predictions, real values and species to file
writefile(AllPR,Spec, path)
