# CNN Regression Model

**This folder includes CNN based regression models to predict optimal growth temperature (ogt) by using tRNA sequences**

## Dependencies:

- Pandas
- Numpy
- Keras
- TensorFlow
- HyperOpt
- Sklearn

Depending on which version of tensorflow the user is working with, these scripts can be run with either CPU or GPU. The source code is identical for to both. 

## Directory contents:
* models directory: contains trained models for running phylogenetic- or distance-split trained models
	* Archaea_dist.h5 and Archaea_dist.json will run the Archaea-only model trained with phylogenetically-informed data training and test sets.
	* Archaea_rand.h5 and Archaea_rand.json will run the Archaea-only model trained with data randomly split into training and test sets.
	* Archaea-Bacteria_dist.h5 and Archaea-Bacteria_dist.json will run the joint Archaea-Bacteria model trained with phylogenetically-informed data training and test sets.
	* Archaea-Bacteria_rand.h5 and Archaea-Bacteria_rand.json will run the joint Archaea-Bacteria model trained with data randomly split into training and test sets.
	* Bacteria_dist.h5 and Bacteria_dist.json will run the Bacteria-only model trained with phylogenetically-informed data training and test sets.
	* Bacteria_rand.h5 and Bacteria_rand.json will run the Bacteria-only model trained with data randomly split into training and test sets.
* scripts directory: contains scripts needed to train a new model or run the existing models on new data
	* RandomSplit-TrainingAndParameterOptimization.py: python script to train a new model with randomly-split training and test datasets
	* PhylogeneticSplit-Training.py: python script to train a new model wtih phylogenetically-split training and test datasets
	* predictiontRNA.py: python script to use an existing model to make OGT predictions for new data


## To run scripts:

The following parameters need to be adjusted to match the file paths and model type being run. These can all be found in the INPUTS section of the scripts. 

### Model type parameters:
* **isBacteriaOn**: 0 if you want to run a model on only Archaea data, 1 otherwise. The joint Archaea-Bacteria model is needed if both isBacteriaOn=1 and isArchaeaOn=1.
* **isArchaeaOn**: 0 if you want to run a model on only Bacteria data, 1 otherwise The joint Archaea-Bacteria model is needed if both isBacteriaOn=1 and isArchaeaOn=1.
* **valRate**: The percentage of the validation set to be held out to optimize parameters. Set at 0.05.
* **numberofChunks**: The number of groups into which to divide data after the validation set is excluded. (numberofChungs - 1 groups will be used for training and 1 group will be used for testing. The process is repeated numberofChunks times, with a different group used for testing each time.) Set at 5.
* **n_clusters**: Number of clusters needed for phylogenetic split of data (parameter not needed for random split). Set at 10.

### CNN/optimization parameters:
* **epochs**: CNN epochs. Set at 30.
* **max_eval**: The number of trials in hyperparameter optimization. Set at 3.
* **ishyperOptNeeded**: If you would like to repeat hyperparameter optimization, set this parameter to 1. Otherwise leave this value at 0 to use the previously-determined optimal parameters.

### Input data specifications:
Note that some parameters are specific to the pre-trained model.

* **trnaSeqColumn**: Column of input data file with tRNA sequences
* **targetColumn**: Column of input data file with known OGT values (only needed when training a new model)
* **speciesColumn**: Column of input data with species information
* **path1**: Path to Archaea input data for training a new model. Path to all input data if making predictions using an existing model.
* **path2**: Path to Bacteria input data.
* **pathDistance**: Path to distance matrix for species in input dataset. Only needed when using the phylogenetically-informed model.
* **ModelPath**: Path to directory containing the trained model (if using an existing model to predict new data)
* **ModelName**: Name of model being used for predictions on new dataset. Will either be "ArchaeaModel_dist", "BacteriaModel_dist", or "Archaea-BacteriaModel_dist" for the phylogenetically-informed model or "ArchaeaModel_rand", "BacteriaModel_rand", or "Archaea-BacteriaModel_rand" for the randomly-split model.

### Output data path: 

* **path**: Path to output files that will be written. 
	* If training a new model, two files are written out. One contains both predictions and real temperatures of the test set and the other contains the best hyper-parameters selected by hyperOpt (if ishyperOptNeeded=1). The script also writes training and test set mean absolute error, pearson r, r squared, and root mean squared error to the console screen.



