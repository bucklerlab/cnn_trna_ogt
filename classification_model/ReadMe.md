# CNN Classification Model

**This folder includes CNN based classification models to predict which of two tRNAs comes from a species wtih higher OGT**

## Dependencies:

- Pandas
- Numpy
- Keras
- TensorFlow
- HyperOpt
- Sklearn

Depending on which version of tensorflow the user is working with, these scripts can be run with either CPU or GPU. The code is identical for both. 

## Directory contents:
* models: contains .h5 and .json files with trained models for running phylogenetic- or distance-split models
	* Distance - models built with phylogenetic distance taken into account when splitting data into training and test sets
		* Archaea
		* Archaea-Bacteria
		* Bacteria
	* Random - models built by randomly assigning observations to training and test sets
		* Archaea
		* Archaea-Bacteria
		* Bacteria
* scripts: contains scripts needed to train a new model
	* Distance-Classification.py
	* Random-Classification.py
* Predictions: 5-fold CV prediction results for each type of model and each dataset. Files are separated into different directories depending on which input data was used for predictions. All files are comma-separated, with columns Species1,OGT1,Species2,OGT2,ModelPrediction. A value of 0 in the ModelPrediction results indicates the first tRNA comes from lower OGT and a value of 1 indicates the second tRNA comes from the lower OGT.
	* Distance: 
		* Archaea
		* Archaea-Bacteria
		* Bacteria
	* Random
		* Archaea
		* Archaea-Bacteria
		* Bacteria


## To run scripts:

The following parameters need to be adjusted to match the file paths and model type being run. These can all be found in the INPUTS section of the scripts. 

### Model type parameters:
* **isBacteriaOn**: 0 if you want to run a model on only Archaea data, 1 otherwise. The joint Archaea-Bacteria model is needed if both isBacteriaOn=1 and isArchaeaOn=1.
* **isArchaeaOn**: 0 if you want to run a model on only Bacteria data, 1 otherwise The joint Archaea-Bacteria model is needed if both isBacteriaOn=1 and isArchaeaOn=1.
* **ValRate**: The percentage of the validation set to be held out to optimize parameters. Set at 0.05.
* **numberofChunks**: The number of groups into which to divide data after the validation set is excluded. (numberofChungs - 1 groups will be used for training and 1 group will be used for testing. The process is repeated numberofChunks times, with a different group used for testing each time.) Set at 5.

### CNN/optimization parameters:
* **max_eval**: The number of trials in hyperparameter optimization. Set at 3.
* **ishyperOptNeeded**: If you would like to repeat hyperparameter optimization, set this parameter to 1. Otherwise leave this value at 0 to use the previously-determined optimal parameters.

### Input data specifications:
Note that some parameters are specific to the pre-trained model.

* **lefttRNAcolumn**: Column of input data file with tRNA sequence 1
* **righttRNAcolumn**: Column of input data file with tRNA sequence 2
* **leftTarget**: Column of input data file with known OGT values 1 (only needed when training a new model)
* **rightTarget**: Column of input data file with known OGT values 2 (only needed when training a new model)
* **min_ogt_diff**: How different should the GOT values be between species 1 and species 2 to be evaluated?
* **path1**: Path to Archaea input data for training a new model. Path to all input data if making predictions using an existing model.
* **path2**: Path to Bacteria input data.

### Output data path: 

* **path**: Path to output files that will be written. 



