import pandas as pd
import numpy as np
from keras import backend as K
import tensorflow as tf
from keras.models import model_from_json
from vis.visualization import visualize_saliency
from vis.utils import utils
from keras import activations
import copy


# We need some global variables. Because couldn't pass them to Hyperopt.

global maxtRNALen
global tRNA
global allSpecies
global YTarget
global path1
global path2

# Tensorflow Configirations
config = tf.ConfigProto(allow_soft_placement=True)
config.gpu_options.per_process_gpu_memory_fraction = 0.4
sess = tf.Session(config=config)
K.set_session(sess)


def elimData(dframe):
    trgt = dframe.values[:, 2]


    cnt1 = (np.abs(trgt - 28.5) > 0.5)
    cnt2 = (np.abs(trgt - 30.5) > 0.5)
    cnt3 = (np.abs(trgt - 37.5) > 0.5)

    cnt = np.logical_and(cnt1, cnt2)
    cnt = np.logical_and(cnt, cnt3)

    res = dframe[cnt]
    return pd.DataFrame(res.values)
def prepareData(isBacteriaOn, isArchaeaOn):
    # Takes two variables that indicates if we use archaea or bacteria
    # Returns read data as pandas data frame

    if isArchaeaOn and isBacteriaOn:
        dfA = readData(path1)
        dfA =elimData(dfA)
        dfB = readData(path2)
        dfB = elimData(dfB)
        d = concatDf([dfA, dfB])
        asize=dfA.shape[0]
        bsize=dfB.shape[0]
    elif isArchaeaOn:
        d = readData(path1)
        d = elimData(d)
        asize=d.shape[0]
        bsize=0
    elif isBacteriaOn:
        d = readData(path2)
        d = elimData(d)
        asize=0
        bsize=d.shape[0]

    return d, asize, bsize

def getTensor(Data, whichColumn):
    # Takes data as pandas data frame
    # Returns selected column as tensor. Selected column should have RNA sequences. Also returns max lenght of the RNA
    SEQs = getColumn(Data, Col=whichColumn)
    mxlen = len(max(SEQs.values[:,0],key=len))
    # Convert SEQs data frame to CNN compatible one-hot encoded.
    SEQTensor = getOneHotEncodedSet(SEQs, mxlen=mxlen)

    return SEQTensor, mxlen


def readData(path, deli="\t", head=0):
    # Takes path and returns read data as pandas data frame
    # head indicates number of rows should be skipped for reading. 0 skips first row. -1 does not skip any row. Optional
    # deli shows delimmiter. Optional.

    return pd.read_csv(path, header=head,delimiter = deli)

def concatDf(dfList):
    # Concats list of data frames into one data frame
    df = dfList[0]
    for l in range(1, len(dfList)):
        df = pd.concat([df, dfList[l]], axis=0)
    return pd.DataFrame(df.values)

def getColumn(df, Col):
    # Takes a pandas data frame and returns a column
    res = df[df.columns[Col]]
    return pd.DataFrame(res.values)
def  getTarget(df,targetColumn):
    # Takes data frame and target column number
    # Returns target as array
    ogt = getColumn(df, Col=targetColumn).values
    ogt=np.array(ogt)
    OGT=[]
    # Not optimal. A better way will be added.
    for a in ogt:
        OGT.append(a[0])
    ogt= np.array(OGT)
    return ogt

def getOneHotEncodedSet(SEQs, mxlen, depth=4):
    # Takes sqquence list, and max lenght of sequences
    # Returns one hot encoded sequence list. Shorter sequences are 0 padded.
    one_hot_seqs = []
    for seq in SEQs.values[:, 0]:
        one_hot_seqs.append(one_hot_encoding(seq, mxlen))

    one_hot_seqs = np.array(one_hot_seqs)
    return  one_hot_seqs.reshape(one_hot_seqs.shape[0], depth, mxlen, 1)
def one_hot_encoding(seq, mx):
    # Takes a sequence and max lenght
    # Returns one hot encoded 2-dimensional array.
    dict = {'A': [1, 0, 0, 0], 'C': [0, 1, 0, 0], 'G': [0, 0, 1, 0], 'T': [0, 0, 0, 1]}
    one_hot_encoded = np.zeros(shape=(4, mx))

    for i, nt in enumerate(seq):

        if nt.upper() == "A" or nt.upper() == "C" or nt.upper() == "G" or nt.upper() == "T" :
            one_hot_encoded[:,i] = dict[nt.upper()]
        else:
            continue
    return one_hot_encoded

def saveModel(model, name):
    # saves a model
    model_json = model.to_json()
    with open(name+".json", "w") as json_file:
        json_file.write(model_json)
    # serialize weights to HDF5
    model.save_weights(name+".h5")
    print("Saved model to disk")
    return
def writeFile(path, ac, spx,lim, trnaidm, x):
    # Writes values to a txt file

    myFile=open(path,'a')

    myFile.write(str(ac) + ","+ str(spx) + ","+ str(lim[0]) + ","+ str(lim[1]) + ","+ str(lim[2]) + ","+ str(lim[3]) + "," + str(lim[4]) + ","+ str(lim[5]) + "," + str(lim[6]) + ","+ str(lim[7])+ ","+ str(lim[8]) + ","+ str(lim[9])+ "," +  str(trnaidm))
    for xx in x:
        myFile.write(","+ str(xx)  )
    myFile.write("\n")
    myFile.close()

    return

def loadModel(name):
    #loads a pre-trained model
    json_file = open(name+".json", 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights(name+".h5")
    print("Loaded model from disk")
    return loaded_model

def sumAccordingtoStructure(gradsO,stO):
    # This function returns base pair attention vector according to grads. and  structure.
    partAll = copy.deepcopy(gradsO[:len(stO)])
    partAllNorm = partAll / np.sum(partAll)

    try:
        st=[]
        grads=[]
        placeInd=[]
        for i in range(len(stO)):
            if stO[i]==">" or  stO[i]=="<":
                st.append(stO[i])
                grads.append((gradsO[i]))
                placeInd.append(i+1)

        count_less=0
        did_see_le=0
        for i in range(len(st)):
            if st[i]=="<":
                did_see_le = 1
                count_less=count_less+1
            if did_see_le ==1 and st[i]==">":
                Dend=placeInd[i-1]
                Anticodonstart=placeInd[i]
                AnticodonstartIndex=i
                break

        count_ge=0
        for i in range(AnticodonstartIndex-1,-1,-1):
            if st[i]==">":
                count_ge=count_ge+1
                Acceptor1start = placeInd[i]
            if count_ge==count_less:
                Dstart = placeInd[i]
            if count_ge == count_less+1:
                Acceptor1end = placeInd[i]



        did_see_le = 0
        for i in range(AnticodonstartIndex,len(st)):
            if st[i] == "<":
                did_see_le = 1
            if did_see_le == 1 and st[i] == ">":
                Anticodonend = placeInd[i - 1]
                Tstart = placeInd[i]
                TstartIndex = i
                break

        did_see_le = 0
        count_ge = 0
        count_le = 0
        for i in range(TstartIndex, len(st)):
            if st[i] ==">" and did_see_le == 0:
                count_ge = count_ge+ 1

            if st[i] == "<":
                did_see_le = 1
                count_le = count_le + 1
                Acceptor2end = placeInd[i]
                if count_le == count_ge:
                    Tend = placeInd[i]
                if count_le == count_ge+1:
                    Acceptor2start = placeInd[i]
        AllList=[]
        AllList.append(partAll)
        AllList.append(partAllNorm)
        valid = 0
        if Anticodonstart < Anticodonend and Acceptor1start < Acceptor1end and Acceptor2start < Acceptor2end and Dstart < Dend and Tstart < Tend:
            valid = 1
        borderlist = []

        borderlist.append(Acceptor1start)
        borderlist.append(Acceptor1end)
        borderlist.append(Dstart)
        borderlist.append(Dend)
        borderlist.append(Anticodonstart)
        borderlist.append(Anticodonend)
        borderlist.append(Tstart)
        borderlist.append(Tend)
        borderlist.append(Acceptor2start)
        borderlist.append(Acceptor2end)
        return valid, AllList, borderlist
    except:
        print("inval")
        return 0, 0, 0



####################################   START     ##############################################################
####################################   INPUTS    ###############################################################

# Work with Archaea, bacteria or combined dataset
isBacteriaOn=1
isArchaeaOn=0

    # Initializations

trnaSeqColumn = 10 # in data file
trnaidColumn = 4
targetColumn = 2 # in data file
speciesColumn = 1 # in data file
AnticodonPlaceColumn = 8

#needs to be updated:
#Input files

#Phylogeny Distance file:
pathDistance = "/users/.../allSpecies_phyloTree_dm.csv"
#archaea and bacteria files:
path1 = "/users/.../Archaea_RNAs_wOGT.csv"
path2 = "/users/.../Bacteria_RNAs_wOGT.csv"



#output file names
outputFileName = ".../random_bacteria-imp_per_bp.txt"
outputFileNameNormalized = ".../random_bacteria-imp_per_bp-normalized.txt"

#Model file Name json and h5: May need to be changed
ModelFilePath= ".../BacteriaModel"

####################################end of input parameters#############################################################
##########################################################################################################################
##########################################################################################################################


# Read data from files
Data,ArcSize,BacSize = prepareData(isBacteriaOn, isArchaeaOn)

mySpecies = Data[Data.columns[speciesColumn]]
myUniqueSpecies = mySpecies.unique()


tRNAs, maxtRNALen = getTensor(Data, trnaSeqColumn)


# Get target OGTs
YTarget = getTarget(Data, targetColumn)
leghts= getTarget(Data, 5)
antiCodons=getTarget(Data, 8)

allSpecies = Data[Data.columns[speciesColumn]]
tRNAIDS = Data[Data.columns[trnaidColumn]]
AnticodonPlace = Data[Data.columns[AnticodonPlaceColumn]]

# loads model
myModel = loadModel(ModelFilePath)



layer_idx = utils.find_layer_idx(myModel, 'dense_6')
myModel.layers[layer_idx].activation = activations.linear
newmodel = utils.apply_modifications(myModel)



stucture=Data.values[:,11]
targets=YTarget.ravel()
nLen=leghts.ravel()
nAnticodon=antiCodons.ravel()
mySp= allSpecies
mySp=np.array(mySp)
tRNAIDm =np.array(tRNAIDS)
AnticodonPlacem=np.array(AnticodonPlace)


sumVec=np.zeros(4)

st=0

for ttt in range(st,len(tRNAs)):
    print(ttt)

    grads = visualize_saliency(newmodel, layer_idx, filter_indices=None, seed_input=tRNAs[ttt, :, :, :],
                               backprop_modifier=None, grad_modifier='absolute')
    grads = np.sum(grads, axis=0)

    isV, aList, my_limits = sumAccordingtoStructure(grads, stucture[ttt])
    if isV == 1:
        writeFile(outputFileName, AnticodonPlacem[ttt],mySp[ttt], my_limits,tRNAIDm[ttt], aList[0])
        writeFile(outputFileNameNormalized , AnticodonPlacem[ttt],mySp[ttt], my_limits, tRNAIDm[ttt], aList[1])







