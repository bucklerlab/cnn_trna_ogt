'''This script reads in a file with tRNA attention statistics from the CNN. For each row in the file, it parses
the columns associated with the Acceptor arm, D arm, Anticodon arm, and T arm. It sums the attention for each arm
in each tRNA and writes this new information to a file.

Columns for the input files are as follows:
AnticodonPosition, Species, Acceptor1Start, Acceptor1End, DStart, DEnd, AnticodonArmStart,
AnticodonArmEnd, TStart, TEnd, Acceptor2Start, Acceptor2End, tRNAid, Attentions (variable lengths)
'''


def main():
    arch_random_file = 'model_attention/random_archaea-atention_per_bp-normalized.txt'
    arch_distance_file = 'model_attention/phylogeny_archaea-atention_per_bp-normalized.txt'
    bact_random_file = 'model_attention/random_bacteria-atention_per_bp-normalized.txt'
    bact_distance_file = 'model_attention/phylogeny_bacteria-atention_per_bp-normalized.txt'
    output_dir = 'model_attention/'

    for in_file in [arch_random_file, arch_distance_file, bact_random_file, bact_distance_file]:
        f = open(in_file, 'r')
        trna_attention = [line.strip('\n').split(',') for line in f]
        f.close()

        attention_sums = ["Species\ttRNAid\tAcceptorAttn\tDArmAttn\tAnticodonAttn\tTArmAttn"]
        for line in trna_attention:
            d_start = int(line[4]) + 13
            d_end = int(line[5]) + 13
            d_attention = float(0)
            for attn in line[d_start:d_end]:
                d_attention += float(attn)
            d_attention_norm = d_attention / (d_end - d_start)

            ant_start = int(line[6]) + 13
            ant_end = int(line[7]) + 13
            ant_attention = float(0)
            for attn in line[ant_start:ant_end]:
                ant_attention += float(attn)
            ant_attention_norm = ant_attention / (ant_end - ant_start)

            t_start = int(line[8]) + 13
            t_end = int(line[9]) + 13
            t_attention = float(0)
            for attn in line[t_start:t_end]:
                t_attention += float(attn)
            t_attention_norm = t_attention / (t_end - t_start)

            acc1_start = int(line[2]) + 13
            acc1_end = int(line[3]) + 13
            acc2_start = int(line[10]) + 13
            acc2_end = int(line[11]) + 13
            acc_attention = float(0)
            for attn1 in line[acc1_start:acc1_end]:
                acc_attention += float(attn1)
            for attn2 in line[acc2_start:acc2_end]:
                acc_attention += float(attn2)
            acc_attention_norm = acc_attention / (acc2_end - acc2_start)

            attention_sums.append(line[1] + '\t' + line[12] + '\t' + str(acc_attention) + '\t' +
                                  str(d_attention) + '\t' + str(ant_attention) + '\t' + str(t_attention))

        if in_file == arch_random_file:
            w = open(output_dir + 'random_archaea-attention_across_tRNAarm-normalized.txt', 'w')
            for line in attention_sums:
                w.write(line + '\n')
            w.close()
        elif in_file == arch_distance_file:
            w = open(output_dir + 'phylogeny_archaea-attention_across_tRNAarm-normalized.txt', 'w')
            for line in attention_sums:
                w.write(line + '\n')
            w.close()
        elif in_file == bact_random_file:
            w = open(output_dir + 'random_bacteria-attention_across_tRNAarm-normalized.txt', 'w')
            for line in attention_sums:
                w.write(line + '\n')
            w.close()
        elif in_file == bact_distance_file:
            w = open(output_dir + 'phylogeny_bacteria-attention_across_tRNAarm-normalized.txt', 'w')
            for line in attention_sums:
                w.write(line + '\n')
            w.close()


if __name__=="__main__":
    main()