'''This script reads in tRNA sequence data used as input for the cnn_trna_ogt model and calculates features
for each tRNA. It then merges this information with the results out put from the model and correlates
each sequence feature with the results.'''

import pandas as pd
from scipy.stats import entropy
import collections
from Bio import SeqIO


def estimate_shannon_entropy(dna_sequence):
    bases = collections.Counter([tmp_base for tmp_base in dna_sequence])
    # define distribution
    dist = [x / sum(bases.values()) for x in bases.values()]

    # use scipy to calculate entropy
    entropy_value = entropy(dist, base=2)

    return entropy_value


def main():
    # Change Bacteria_RNAs_wOGT.txt to ArchaeaRNAs_wOGT.txt uncomment commented ogt_predictions line, and comment out current ogt_predictions line to
    # get the same statistics for the archaea dataset
    input_trnas = "data/input_data/Bacteria_RNAs_wOGT.txt"
    # ogt_predictions = "regression_model/Predictions/Random/Archaea/tRNAidFinal-Random-Bacteria-0-archaea-1-isRandomSplit-1--chunks-5-ValRate-0.0.txt"
    ogt_predictions = "regression_model/Predictions/Random/Bacteria/tRNAidFinal-Random-Bacteria-1-archaea-0-isRandomSplit-1--chunks-5-ValRate-0.0.txt"
    trna_mfes = "model_attention/data/Bacteria_tRNA_sequenceMFE.txt"

    trnas = pd.read_csv(input_trnas, sep='\t')
    predicted_ogt = pd.read_csv(ogt_predictions, sep=',', names=["Species", "tRNAid", "TrueOGT", "PredictedOGT"])
    trna_ogts = pd.merge(trnas, predicted_ogt, how="inner", left_on=['Species', 'tRNAid'], right_on=['Species', 'tRNAid'])
    trna_ogts['OGTerror'] = trna_ogts['TrueOGT'] - trna_ogts['PredictedOGT']
    trna_ogts['ShannonEntropy'] = trna_ogts['tRNA_Sequence'].apply(lambda x: estimate_shannon_entropy(x.upper()))
    trna_ogts['GCcontent'] = trna_ogts['tRNA_Sequence'].apply(lambda x: (collections.Counter(x.upper())['C'] +
                              collections.Counter(x.upper())['G']) / len(x))

    # Parse and correlate MFE to OGT error:
    mfe_info = []
    for record in SeqIO.parse(trna_mfes, 'fasta'):
        mfe_info.append(str(record.id).split('_')[0] + '_' + str(record.id).split('_')[1] + ',' +
                        str(record.id).split('_')[2] + ',' +
                        str(record.seq).split('(')[-1].split(')')[0])
    mfe_info_splt = [line.strip('\n').split(',') for line in mfe_info]

    mfe_info_df = pd.DataFrame(mfe_info_splt, columns=['Species', 'tRNAid', 'MFE'])
    mfe_info_df['MFE'] = mfe_info_df['MFE'].astype(float)
    trna_ogts = pd.merge(trna_ogts, mfe_info_df, how="left", on=['Species', 'tRNAid'])

    trna_trueogt = pd.to_numeric(trna_ogts['TrueOGT'], downcast="float", errors="coerce")
    trna_predogt = pd.to_numeric(trna_ogts['PredictedOGT'], downcast="float", errors="coerce")
    trna_mfes = pd.to_numeric(trna_ogts['MFE'], downcast="float", errors="coerce")
    trna_gc = pd.to_numeric(trna_ogts['GCcontent'], downcast="float", errors="coerce")

    trna_corrs = pd.DataFrame()
    trna_corrs['PredictedOGT'] = trna_predogt
    trna_corrs['MFE'] = trna_mfes
    trna_corrs['GC'] = trna_gc
    # trna_corrs.to_csv("model_attention/data/Bacteria_OGTpredictionCorrelations.csv", sep=',', index=False)

    # Print statistics
    print("Mean tRNA length: " + str(trna_ogts['Length'].mean()))
    print("Mean OGT error: " + str(trna_ogts['OGTerror'].mean()))
    print("Mean Shannon entropy: " + str(trna_ogts['ShannonEntropy'].mean()))
    print("Mean GC content: " + str(trna_ogts['GCcontent'].mean()))
    print("Mean MFE: " + str(trna_ogts['MFE'].mean()))

    ## Correlations between true and predicted OGT values and MFE
    print("Correlation between true OGT and MFE")
    print(trna_trueogt.corr(trna_mfes))

    print("Correlation between predicted OGT and MFE")
    print(trna_predogt.corr(trna_mfes))

    print("Correlation between true OGT and GC content")
    print(trna_trueogt.corr(trna_gc))

    print("Correlation between predicted OGT and GC content")
    print(trna_predogt.corr(trna_gc))

    ## Values for individual tRNAs
    print("Correlation between delta OGT and tRNA length for each tRNA")
    print(abs(trna_ogts['OGTerror']).corr(trna_ogts['Length']))

    print("Correlation between delta OGT and Shannon entropy of tRNAs")
    print(abs(trna_ogts['OGTerror']).corr(trna_ogts['ShannonEntropy']))

    print("Correlation between delta OGT and GC content of each tRNA")
    print(abs(trna_ogts['OGTerror']).corr(trna_ogts['GCcontent']))

    print("Correlation between OGT error and MFE of tRNAs")
    print(abs(trna_ogts['OGTerror']).corr(trna_ogts['MFE']))
    
    ## Mean across all tRNAs in a species
    species_mean = trna_ogts.groupby('Species').mean()

    print("Correlation between mean(delta OGT) and how many tRNAs are in the species")
    trna_counts = trna_ogts['Species'].value_counts().rename_axis('Species').reset_index(name='Counts')
    trna_ogt_counts = pd.merge(species_mean, trna_counts, on='Species')
    print(abs(trna_ogt_counts['OGTerror']).corr(trna_ogt_counts['Counts']))
    # print(trna_ogt_counts['Counts'].mean())

    print("Correlation between mean(delta OGT) and mean(tRNA length)")
    print(abs(species_mean['OGTerror']).corr(species_mean['Length']))

    print("Correlation between mean(delta OGT) and mean(Shannon Entropy)")
    print(abs(species_mean['OGTerror']).corr(species_mean['ShannonEntropy']))

    print("Correlation between mean(delta OGT) and mean(GCcontent)")
    print(abs(species_mean['OGTerror']).corr(species_mean['GCcontent']))

    print("Correlation between mean(delta OGT) and mean(MFE)")
    print(abs(species_mean['OGTerror']).corr(species_mean['MFE']))



if __name__ == '__main__':
    main()