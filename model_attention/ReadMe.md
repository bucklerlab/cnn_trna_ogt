# Understanding model attention

Script to write out model attention and evaluate attention based on region of tRNA input sequence.

# Directory contents:
* data: files produced to compare model attention statistics
	* Archaea_OGTpredictionCorrelations.csv: correlations between MFE, GC content, and OGT predictions for archaea
	* Archaea_tRNA_sequenceMFE.txt: tRNA sequences and MFE values calculated with ViennaRNA for archaea
	* Bacteria_OGTpredictionCorrelations.csv: correlations between MFE, GC content, and OGT predictions for bacteria
	* Bacteria_tRNA_sequenceMFE.txt: tRNA sequences and MFE values calculated with ViennaRNA for bacteria
	* Ecoli_disruption_trna_withLoopOGT_predictionsMFE.txt: results from mutating single bases within E. coli tRNAs
	* phylogeny_archaea-attention_per_bp-normalized.txt: comma-delimited attention file for the phylogenetically-split regression model using Archaea data. 
		* Columns in the txt files are as follows: Anticodon place, species name, Acceptor (L) start, Acceptor (L) end, D arm start, D arm end, Anticodon start, Anticodon end, T arm start, T arm end, Acceptor (R) start, Acceptor (R) end, tRNA id, attention per base vector (lengths are different due to tRNA sequence)
	* phylogeny_archaea-attention_across_tRNAarm-normalized.txt: attention file for the phylogenetically-split regression model using archaea data, summed across each tRNA structure separately
	* phylogeny_bacteria-attention_per_bp-normalized.txt: comma-delimited attention file for the phylogenetically-split regression model using Bacteria data. 
		* Columns in the txt files are as follows: Anticodon place, species name, Acceptor (L) start, Acceptor (L) end, D arm start, D arm end, Anticodon start, Anticodon end, T arm start, T arm end, Acceptor (R) start, Acceptor (R) end, tRNA id, attention per base vector (lengths are different due to tRNA sequence)
	* phylogeny_bacteria-attention_across_tRNAarm-normalized.txt: attention file for the phylogenetically-split regression model using bacteria data, summed across each tRNA structure separately
	* random_archaea-attention_per_bp-normalized.txt: comma-delimited attention file for the randomly-split regression model using Archaea data. 
		* Columns in the txt files are as follows: Anticodon place, species name, Acceptor (L) start, Acceptor (L) end, D arm start, D arm end, Anticodon start, Anticodon end, T arm start, T arm end, Acceptor (R) start, Acceptor (R) end, tRNA id, attention per base vector (lengths are different due to tRNA sequence)
	* random_archaea-attention_across_tRNAarm-normalized.txt: attention file for the randomly-split regression model using archaea data, summed across each tRNA structure separately
	* random_bacteria-attention_per_bp-normalized.txt: comma-delimited attention file for the randomly-split regression model using Bacteria data.  
		* Columns in the txt files are as follows: Anticodon place, species name, Acceptor (L) start, Acceptor (L) end, D arm start, D arm end, Anticodon start, Anticodon end, T arm start, T arm end, Acceptor (R) start, Acceptor (R) end, tRNA id, attention per base vector (lengths are different due to tRNA sequence)
	* random_bacteria-attention_across_tRNAarm-normalized.txt: attention file for the randomly-split regression model using bacteria data, summed across each tRNA structure separately
	* tRNA-AApredictions-Phylogeny-Archaea.txt: OGT prediction for each tRNA, labeled by amino acid
	* tRNA-AApredictions-Phylogeny-Bacteria.txt: OGT prediction for each tRNA, labeled by amino acid
	* tRNA-AApredictions-Random-Archaea.txt: OGT prediction for each tRNA, labeled by amino acid
	* tRNA-AApredictions-Random-Bacteria.txt: OGT prediction for each tRNA, labeled by amino acid
* scripts
	* Attention.py: script to calculate model attention for each base in the tRNA sequence
	* calcFeaturePredictiveness.py: script to calculate and print correlations between tRNA features and OGT values
	* getTrnaArmAttention.py: script to consolidate output from Attention.py to assign attention to each tRNA arm

# To re-run script to find model attention:

Adjust the parameters below depending on which dataset is being used and run Attention.py to calculate the attention paid each nucleotide in the tRNA.

## Dependencies:

- Pandas
- Numpy
- Tensorflow
- Keras
- Keras - vis (https://raghakot.github.io/keras-vis/) 

## Dataset parameters
* **isBacteriaOn** - 1 if using Bacteria species in the dataset, 0 otherwise
* **isArchaeaOn** - 1 if using Archaea species in the dataset, 0 otherwise
* **trnaSeqColumn** - column in input data file with tRNA sequence
* **targetColumn** - column in input data file with OGT information
* **speciesColumn** - column in input data file with species information
* **AnticodonPlaceColumn** - column in input data file with anticodon place

## Path values
* **pathDistance** - path to phylogenetic distance tree file
* **path1** - path to Archaea tRNA file
* **path2** - path to Bacteria tRNA file
* **outputFileName** - path and name of desired attention output file 
* **outputFileNameNormalized** - path and name of desired normalized attention output file 
* **ModelFilePath** - path and name of the trained model (name of h5 and json files)

