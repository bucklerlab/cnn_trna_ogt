# Figures for paper

Script and figures for OGT-prediction models from tRNA sequence.

# Directory contents:
* Figure1.png: tRNA MFE structures
* Figure2.pdf: CNN model structures
* Figure3.pdf: Classification model results
* Figure4.pdf: Regression model results
* Figure5.pdf: Regression model attention statistics for archaea phylogeny model
* FigureS1.pdf: OGT and GC content correlations
* FigureS2.pdf: Amino acid correlations
* FigureS3.pdf: Regression model attention statistics for all other models

# To re-run scripts:
* `Rscript CNN_attention_significance.R`

