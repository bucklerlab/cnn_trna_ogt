# Predicting Optimal Growth Temperature (OGT) for microbes using a CNN

This repository consists of data, source code and supplementary files for the study titled "Building tRNA thermometer to estimate microbial adaptation to temperature". The classification model predicts which of two tRNAs comes from a species with higher OGT. The regression model predicts Optimal Growth Temperature (OGT) of prokaryotes based on tRNA sequences.

# Repository contents:

* classification_model: scripts to train the OGT classification model or use the existing model to predict OGT for new data
* regression_model: scripts to train the OGT regression prediciton model or use the existing model to predict OGT for new data
* data: input data files with species, OGT, and tRNA info
* model_attention: directory containing scripts used to evaluation attention for the regression model
* paper_figures: directory with scripts needed to make figures in paper

# Steps to set up and run scripts:

All scripts used to create and run the CNN models were run in a Conda environment. To create a new conda environment, install the following packages and update the scripts to match local directory structure.

## To create and run from new conda environment:
1. Create new conda environment with python 3.6: `conda create --name CNN_tRNA_OGT python=3.6`
2. Activate environment: `conda activate CNN_tRNA_OGT`
3. Install sklearn: `conda install -c anaconda scikit-learn`
4. Install hyperopt: `conda install -c conda-forge hyperopt`
5. Install pandas: `conda install -c conda-forge pandas`
6. Install scipy: `conda install -c anaconda scipy`
7. Install tensorflow 1.12.0: `conda install -c anaconda tensorflow-gpu=1.12.0`
8. Install Keras: `conda install -c anaconda keras-gpu`
9. Update parameters and paths in _INPUTS_ section of script.
10. Run: `python3 [model script]` for your selected model

# Steps to run a pre-trained model on new dataset:
1. Find the corrrect model within this repository and download both the \*.h5 and \*.json files
2. Update trnaSeqColumn index (column with tRNA sequences in input file), speciesColumn index (column with species name in input file), output file path (path variable), tRNA file path (path1 variable), and trained model path and name (ModelPath and ModelName variables). ModelName will be one of "ArchaeaModel", "Archaea-BacteriaModel", or "BacteriaModel"
3. Run `python3 predictiontRNA.py`. Note that this will run into problems if any tRNAs in the new dataset are longer than tRNAs in the trained model.


Contact:

Emre Cimen: ecimen@eskisehir.edu.tr


Sarah E. Jensen: sej65@cornell.edu