# Data pre-processing steps for OGT prediction model

This directory contains scripts used to produce input data for the CNN tRNA thermometer model, as well as the input data file produced by those scripts.

## Contents:
* scripts: directory with files to produce input data files
	* 0_tRNAscanSE_parallel.sh: bash script to process genome assemblies and predict tRNAs in parallel
	* 1_findAndExtractRNAs.sh: bash script to process genome assemblies and predict rRNAs in each genome
	* 2_readTrnaScanStructureFile.py: python3 script to parse tRNAscan-SE output file with tRNA sequences and format
	* 3_selectGenomeAssembly.py: python3 script to parse tRNA and rRNA files to choose best genome assembly for each species
	* 4_combineSpeciesInfo.py: python3 script to combine information from tRNAs and OGT files into a single file
	* 5_getTaxids.py: python3 script to output a file with taxids for each species
	* 6_createDistMxFromTree.R: R script to create a distance matrix from NCBI Common Tree
* input_data: directory with files used to train models
	* Archaea_RNAs_wOGT.txt.zip: tRNA data file for Archaea, used in Regression model
	* Archaea_tRNApairs_wOGT.txt.zip: tRNA data file for Archaea, used in Classification model
	* Bacteria_RNAs_wOGT.txt.zip: tRNA data file for Bacteria, used in Regression model
	* Bacteria_tRNApairs_wOGT_100Ksample.txt.zip: tRNA data file for Bacteria, used in Classification modela

## To re-run:
1. Download all genomes that you want to use with the model and put them in a single directory.
2. Run files in the order listed. Instructions for each file are included as comments in the file header. Steps 5-6 are only needed for phylogenetic split.


