'''Takes in list of species and taxids from genbank records and outputs a file with taxids for species with OGT.'''


import sys
import getopt
import pandas as pd


def getTaxidsForSpeciesList(names_file, species_list, taxid_output):
    taxid_names = pd.read_csv(names_file, sep='\t|\t', engine='python')
    species = pd.read_csv(species_list, sep='\t')
    taxid_names["Species"] = taxid_names["all"].str.replace(' ', '_')
    species_taxid = pd.merge(taxid_names, species, on="Species")

    header = ['1', 'Species']
    species_taxid.to_csv(taxid_output, sep='\t', index=False, columns=header)



def main(argv):
    names_infile = ''
    taxid_outfile = ''
    selected_species = ''
    try:
        opts, args = getopt.getopt(argv, "hi:s:o:", ["names_infile", "selected_species_list" "taxid_outfile"])
    except getopt.GetoptError:
        print('3_selectGenomeAssembly.py -i <names_infile> -s <selected_species_list> -o <taxid_outfile>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('7_getTaxids.py -i <names_infile> -s <selected_species_list> -o <taxid_outfile>')
            sys.exit()
        elif opt in ("-i", "--names_infile"):
            names_infile = arg
        elif opt in ("-o", "--taxid_outfile"):
            taxid_outfile = arg
        elif opt in ("-s", "--species_list"):
            selected_species = arg
    print('Input names file is: ', names_infile)
    print('Species list is: ', selected_species)
    print('Taxid output file: ', taxid_outfile)

    getTaxidsForSpeciesList(names_infile, selected_species, taxid_outfile)


if __name__ == "__main__":
    main(sys.argv[1:])