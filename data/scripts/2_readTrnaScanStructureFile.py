"""
This script takes in the structure file (produced with the -f flag from tRNAscanSE) for a species and parses it.

It expects a set of files in the format output by tRNAscanSE -f flag, with one file for each genome assembly. The
script will read in all files in a given directory, provided they are named with "_structure.txt" at the end of the
file name. It then parses the information in that file and calculates GC content and shannon entropy. Results are
written to a tab-delimited file.

Inputs:
    - path to directory with tRNAscanSE structure output files
    - name of file to write

Outputs:
    - single tab-delimited file with tRNAid, tRNA Cove score, species, tRNA length, tRNA GC content, tRNA Shannon
    entropy, anticodon sequence, amino acid, sequence, structure, and domain.

Preconditions:
    - All structure files in the input directory have a common string that can be used to identify them.
"""

import sys
import os
import re
import getopt


def parseTrnaInfo(chunk, species, trna_list, filename):
    new_trna = chunk
    for line in new_trna:
        if "Length" in line:
            line1 = re.split('\s+', line)
            trna_name = str(line1[0] + line1[1])
        elif line.startswith('Type:'):
            line2 = re.split('\s+', line)
            amino_acid = line2[1]
            anticodon = line2[3]
            anticodon_pos = line2[5]
            cove_score = line2[8]
        elif line.startswith('Seq:'):
            line3 = re.split('\s+', line)
            sequence = line3[1]
            trna_length = len(sequence)
        elif line.startswith('Str:'):
            line4 = re.split('\s+', line)
            structure = line4[1]
    trna_list.append(filename + '\t' + species + '\t' + trna_name + '\t' + str(trna_length) + '\t' + amino_acid + '\t'
                     + anticodon + '\t' + anticodon_pos + '\t' + cove_score + '\t' + sequence + '\t' + structure)
    # return new_trna


def splitFileToTrnas(infile, species, trna_list, filename):
    '''Note that we expect a minimum of 5 lines for each tRNA from tRNAscanSE (7 lines if there is an intron). These
    expected chunk sizes are hardcoded into this function.'''
    count, chunk_no, chunk_count, chunk = 1, 1, 0, []
    with open(infile, 'r') as f:
        for row in f:
            if count > 4 and row == "\n":
                chunk_count += 1
                count = 1
                parseTrnaInfo(chunk, species, trna_list, filename)
                chunk = []
                chunk_count = 0
                chunk_no += 1
            else:
                count += 1
                chunk.append(row)


def readFiles(in_dir, common_string):
    filelist = []
    trna_list = ['FileName\tSpecies\ttRNAid\tLength\tAminoAcid\tAnticodon\tAnticodonPosition\tCoveScore\tSequence\tStructure']
    for file in os.listdir(in_dir):
        if file.endswith(common_string):
            print(os.path.join(in_dir, file))
            filelist.append(str(in_dir) + str(file))
    for value in filelist:
        filename = str(value.split("/")[-1])
        print("Processing... " + filename)
        species = str(value.split("/")[-1].split(".")[0].split('_')[0] + '_' + value.split("/")[-1].split(".")[0].split('_')[1])
        splitFileToTrnas(value, species, trna_list, filename)
    return trna_list


def main(argv):
    in_dir = ''
    common_string = ''
    outfile = ''
    try:
        opts, args = getopt.getopt(argv, "hi:s:o:", ["in_dir", "common_string", "out_file"])
    except getopt.GetoptError:
        print('2_readTrnaScanStructureFile.py -i <input_dir> -s <common_string> -o <output_file>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('2_readTrnaScanStructureFile.py -i <input_dir> -s <common_string> -o <output_file>')
            sys.exit()
        elif opt in ("-i", "--in_dir"):
            in_dir = arg
        elif opt in ("-s", "--common_string"):
            common_string = arg
        elif opt in ("-o", "--out_file"):
            outfile = arg
    print('Input directory is: ', in_dir)
    print('Common string is: ', common_string)
    print('Output file is: ', outfile)

    trna_list = readFiles(in_dir, common_string)

    w = open(outfile, 'w')
    for line in trna_list:
        w.write(line + '\n')
    w.close()



if __name__ == "__main__":
    main(sys.argv[1:])

