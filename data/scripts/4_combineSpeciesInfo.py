'''This script combines all information from tRNAs, rRNA consensus, and OGT files into a single file.
It is meant to be run after script 5_createConsensusRrnas.py.'''

import sys
import getopt
import os
from Bio import SeqIO
import pandas as pd


def addOgtInfo(merged_rna_df, ogt_data_path):
    species_names = merged_rna_df['Species'].str.lower()
    merged_rna_df['LowerNames'] = species_names

    ogt = pd.read_csv(ogt_data_path, sep=',', names=['Species_Sauer', 'OGT', 'OGT_Source'])
    rna_ogt = pd.merge(merged_rna_df, ogt, left_on='LowerNames', right_on='Species_Sauer')
    return rna_ogt


def combineRnaInputs(trna_file, rrnas_16s, rrnas_23s):
    trnas = pd.read_table(trna_file, sep='\t')
    trna_filenames = trnas['FileName'].str.split('_structure.txt', n=1, expand=True)
    trnas['GenomeAssembly'] = trna_filenames[0] +".fa"
    small_subunit = [line.split(',') for line in rrnas_16s]
    large_subunit = [line.split(',') for line in rrnas_23s]

    s16 = pd.DataFrame(small_subunit, columns=['GenomeAssembly', '16s_rRNA'])
    s23 = pd.DataFrame(large_subunit, columns=['GenomeAssembly', '23s_rRNA'])

    joint_rRNAs = pd.merge(s16, s23, on='GenomeAssembly')
    all_rnas = pd.merge(trnas, joint_rRNAs, on='GenomeAssembly')
    return all_rnas


def concatenateRrnasToList(rrna_dir):
    s16_rRNAs = []
    s23_rRNAs = []
    for file in os.listdir(rrna_dir):
        if file.endswith("_16s.fa"):
            with open(rrna_dir + file, "r") as handle:
                for record in SeqIO.parse(handle, "fasta"):
                    s16_rRNAs.append(str(file).split('/')[-1].split("_16s.fa")[0] + '.fa,' + str(record.seq))
        if file.endswith("23s.fa"):
            with open(rrna_dir + file, "r") as handle:
                for record in SeqIO.parse(handle, "fasta"):
                    s23_rRNAs.append(str(file).split('/')[-1].split("_23s.fa")[0] + '.fa,' + str(record.seq))

    return s16_rRNAs, s23_rRNAs


def main(argv):
    tRNA_list = ''
    rRNA_consensus_dir = ''
    ogt_data_file = ''
    output_file = ''
    try:
        opts, args = getopt.getopt(argv, "ht:r:d:o:", ["tRNA_list", "rRNA_consensus_dir", "ogt_data" "output_file"])
    except getopt.GetoptError:
        print('6_combineSpeciesInfo.py -t <tRNA_list> -r <rRNA_consensus_dir> -d <ogt_data> -o <output_file>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('6_combineSpeciesInfo.py -t <tRNA_list> -r <rRNA_consensus_dir> -d <ogt_data> -o <output_file>')
            sys.exit()
        elif opt in ("-t, --tRNA_list"):
            tRNA_list = arg
        elif opt in ("-r", "--rRNA_consensus_dir"):
            rRNA_consensus_dir = arg
        elif opt in ("-d", "--ogt_data"):
            ogt_data_file = arg
        elif opt in ("-o", "--output_file"):
            output_file = arg
    print('Input directory is: ', tRNA_list)
    print('rRNA directory is: ', rRNA_consensus_dir)
    print('OGT data file is: ', ogt_data_file)
    print('rRNA output file is: ', output_file)

    s16_test, s23_test = concatenateRrnasToList(rRNA_consensus_dir)
    combined_rnas = combineRnaInputs(trna_file=tRNA_list, rrnas_16s=s16_test, rrnas_23s=s23_test)
    rnas_ogt = addOgtInfo(combined_rnas, ogt_data_file)

    header = ['GenomeAssembly', 'Species', 'OGT', 'OGT_Source', 'tRNAid', 'Length', 'AminoAcid', 'Anticodon',
              'AnticodonPosition', 'CoveScore', 'Sequence', 'Structure', '16s_rRNA', '23s_rRNA']
    rnas_ogt.to_csv(output_file, sep='\t', index=False, columns=header)


if __name__ == "__main__":
    main(sys.argv[1:])