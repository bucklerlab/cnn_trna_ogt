"""
This script takes in the tab-delimited file produced by 2_readTrnaScanStructureFile.py and subsets it to a list of
unique genomes.

It expects a tab-delimited file from 2_readTrnaScanStructureFile.py. This file contains information on tRNAs from all
 genomes downloaded from Ensembl. This script identifies which genomes are duplicates from the same species, and finds
 the genome with the largest number of predicted tRNAs. It then finds the rRNA file corresponding to that genome
 assembly, and double checks that all three subunits have been predicted. If they have, it writes information from this
 genome assembly to a new file.

 TODO: deal with assemblies with missing rRNAs
"""


import sys
import getopt
import collections
import pandas as pd
from Bio import SeqIO


def writeRrnasFromSelectedGenomes(genome_pass_list, rRNA_input_dir, rRNA_output_dir):
    selected_genome_list = []
    genome_name_list = []
    for line in genome_pass_list:
        if line.split(',')[1] not in selected_genome_list:
            selected_genome_list.append(line.split(',')[1])
            genome_name_list.append(line.split(',')[0])

    for line in genome_name_list:
        subunit_16s = []
        subunit_23s = []
        for record in SeqIO.parse(rRNA_input_dir + line + '.fa_rnaSubunits.fa', "fasta"):
            if "16S" in record.id.split('Name=')[1].split('_')[0]:
                subunit_16s.append(">" + str(record.id) + '\n' + str(record.seq))
            if "23S" in record.id.split('Name=')[1].split('_')[0]:
                subunit_23s.append(">" + str(record.id) + '\n' + str(record.seq))
        for value in ["16s", "23s"]:
            if len(eval("subunit_" + value)) == 1:
                w = open(rRNA_output_dir + 'Consensus_rRNAs/' + line + '_' + value + '.fa', 'w')
                for entry in eval("subunit_" + value):
                    w.write(entry + '\n')
                w.close()
            else:
                w = open(rRNA_output_dir + 'Multiple_rRNAs/' + line + '_' + value + '.fa', 'w')
                for entry in eval("subunit_" + value):
                    w.write(entry + '\n')
                w.close()
    return genome_name_list


def compareTrnaRrnaPredictions(genome_list_df, rrna_dir):
    pass_list = []
    fail_list = []
    for row in genome_list_df.itertuples():
        if row[2] == row[4]:
            rrna_subunits = []
            fasta = row[1].split('_structure.txt')[0]
            species = row[3]
            for record in SeqIO.parse(rrna_dir + fasta + '.fa_rnaSubunits.fa', "fasta"):
                rrna_subunits.append(record.id.split('Name=')[1].split('_')[0])
            if "16S" in rrna_subunits and "23S" in rrna_subunits:
                pass_list.append(fasta + ',' + species)# + ',' + row[3])
            else:
                fail_list.append(fasta)
    return pass_list, fail_list


def getGenomeTrnaCounts(trna_file):
    genome_names = []
    for line in trna_file[1:]:
        genome_names.append(line[0])

    genome_counts = collections.Counter(genome_names)
    genome_list = []
    for key, value in genome_counts.items():
        genome_split = key.split('_')
        if genome_split[0] == '':
            species = str(genome_split[1] + '_' + genome_split[2].split('.')[0])
        else:
            species = str(genome_split[0] + '_' + genome_split[1].split('.')[0])
        genome_list.append(str(key) + '\t' + str(value) + '\t' + species)

    genome_list_split = []
    for line in genome_list:
        genome_list_split.append(line.split('\t'))

    genome_list_df = pd.DataFrame(data=genome_list_split, columns=['Genome', 'Count', 'Species'])
    genome_list_df['Max_tRNAs'] = genome_list_df.groupby(['Species'])['Count'].transform(max)

    return genome_list_df


def main(argv):
    trna_in_file = ''
    rrna_in_dir = ''
    rna_out_dir = ''
    try:
        opts, args = getopt.getopt(argv, "hi:r:d:", ["trna_in_file", "rRNA_in_dir", "genome_out_file", "rna_out_dir"])
    except getopt.GetoptError:
        print('3_selectGenomeAssembly.py -i <trna_input_file> -r <rRNA_input_directory> -d <rRNA_output_directory>')
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print('3_selectGenomeAssembly.py -i <trna_input_file> -r <rRNA_input_directory> -d <rRNA_output_directory>')
            sys.exit()
        elif opt in ("-i", "--trna_in_file"):
            trna_in_file = arg
        elif opt in ("-r", "--rRNA_dir"):
            rrna_in_dir = arg
        elif opt in ("-d", "--rRNA_out_dir"):
            rna_out_dir = arg
    print('Input tRNA file is: ', trna_in_file)
    print('rRNA directory is: ', rrna_in_dir)
    print('rRNA output directory is: ', rna_out_dir)

    f = open(trna_in_file, 'r')
    trnas = [line.strip('\n').split('\t') for line in f]
    f.close()

    genome_counts = getGenomeTrnaCounts(trnas)
    pass_genomes, fail_genomes = compareTrnaRrnaPredictions(genome_counts, rrna_in_dir)
    genome_names = writeRrnasFromSelectedGenomes(pass_genomes, rrna_in_dir, rna_out_dir)

    w = open(rna_out_dir + 'selected_genomes.txt', 'w')
    for line in genome_names:
        w.write(line + '\n')
    w.close()

    y = open(rna_out_dir + 'genomes_missing_rRNAs.txt', 'w')
    for line in fail_genomes:
        y.write(line + '\n')
    y.close()


if __name__ == "__main__":
    main(sys.argv[1:])