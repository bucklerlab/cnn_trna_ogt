#!/usr/bin/env bash

# This script loops through all files in the input directory and uses tRNAscan-SE (v.2.0.3) to predict tRNA sequences.
# Requires: parallel, tRNAscan
# Assumes that script is run from within the directory containing all genome assemblies.

in_dir=''
tmp_dir=''
in_parallel=''
domain=''
suffix=''

help_message() {
    printf "Usage:
    bash 0_tRNAscanSE_parallel.sh -i <input_directory> -s <assembly_file_suffix> -t <temporary_file_dir> -j <j_parallel_files> -d <domain> \n\

-i  input directory with genome assemblies. Output files will also be written to this directory.
-s  genome assembly file suffixes. This must be consistent for all files in the directory (e.g. '.fa' or '.fasta'.)
-t  temporary directory (needed for parallel processing)
-j  number of files to process in parallel
-d  domain of species in input directory. Should be either 'A' for archaea or 'B' for bacteria\n"
}

while getopts 'hi:s:t:j:d:' flag; do
  case "${flag}" in
    i) in_dir="${OPTARG}" ;;
    s) suffix="${OPTARG}" ;;
    t) tmp_dir="${OPTARG}" ;;
    j) in_parallel="${OPTARG}" ;;
    d) domain="${OPTARG}" ;;
    h) help_message
        exit 1 ;;
  esac
done

cd ${in_dir}

time /programs/parallel/bin/parallel --tmpdir ${tmp_dir} -j ${in_parallel} tRNAscan-SE -${domain} -o {/.}.txt -f {/.}_structure.txt -m {/.}_stats.txt {/.}.fa ::: $(find ${in_dir} -type f -name "${suffix}")
