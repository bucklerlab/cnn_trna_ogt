#!/usr/bin/env bash

# This script loops through files in a directory and uses barrnap (v0.9) and bedtools (v2.27.1) to find rRNA sequences.
# Assumes the script is run from within the same directory as the genome assembly files
# Requires: barrnap, bedtools, awk


in_dir=''
domain=''
suffix=''
threads=''

help_message() {
    printf "Usage:
    bash 0_tRNAscanSE_parallel.sh -i <input_directory> -s <assembly_file_suffix> -d <domain> -t <threads> \n\

-i  input directory with genome assemblies. Output files will also be written to this directory.
-s  genome assembly file suffixes. This must be consistent for all files in the directory (e.g. '.fa' or '.fasta'.)
-d  domain of species in input directory. Should be either 'arc' for archaea or 'bac' for bacteria
-t  number of threads\n"
}

while getopts 'hi:s:d:t:' flag; do
  case "${flag}" in
    i) in_dir="${OPTARG}" ;;
    s) suffix="${OPTARG}" ;;
    d) domain="${OPTARG}" ;;
    t) threads="${OPTARG}" ;;
    h) help_message
        exit 1 ;;
  esac
done

cd ${in_dir}
mkdir RNA_subunit_beds
mkdir RNA_subunit_fastas

filenames=`ls ${suffix}`

for file in $filenames
do

echo $file

barrnap --kingdom ${domain} --threads ${threads} ${file} | awk '{print $1"\t"$4"\t"$5"\t"$9"\t0\t"$7}' > ../RNA_subunit_beds/${file}_rnaSubunits.bed

bedtools getfasta -s -name+ -fi ./${file} -bed ./RNA_subunit_beds/${file}_rnaSubunits.bed -fo ./RNA_subunit_fastas/${file}_rnaSubunits.fa

done